# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/22 15:11:54 by wto               #+#    #+#              #
#    Updated: 2018/03/22 15:11:55 by wto              ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import sys
from game import Game
from inspect import signature

def automate_player(game, instructions):
	game.draw()
	instruction = [s.strip() for s in instructions.readline().split(' ')]
	if instruction[0] != "Nothing":
		method = getattr(Game, instruction[0])
		num_args = len(signature(method).parameters)
		if num_args > 0:
			method(game, *([int(s) for s in instruction[1:num_args]]))
		else:
			method(game)

if __name__ == "__main__":
	if len(sys.argv) == 3:
		game = Game(sys.argv[1]);
		with open(sys.argv[2], 'r') as instructions:
			game.place_wave()
			automate_player(game, instructions)
			while (not game.is_over()):
				game.run_turn()
				if (not game.is_over()):
					automate_player(game, instructions)
			print("Game Over");
