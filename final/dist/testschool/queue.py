# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    queue.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/22 15:11:37 by wto               #+#    #+#              #
#    Updated: 2018/03/22 15:11:37 by wto              ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

class Queue:
	def __init__(self):
		self.list = []

	def peek(self):
		if len(self.list) > 0:
			return self.list[0]
		return None

	def enqueue(self, item):
		self.list.append(item)

	def dequeue(self):
		return self.list.pop(0)

	def size(self):
		return len(self.list)

	def is_empty(self):
		return self.size() == 0

	def combine(self, q):
		while (not q.is_empty()):
			self.enqueue(q.dequeue());
