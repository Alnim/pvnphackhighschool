# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    nonPlant.py                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/22 15:10:52 by wto               #+#    #+#              #
#    Updated: 2018/04/05 10:48:01 by mray             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


from organism import Organism

class Non_Plant(Organism):

	worth = 20
	def __init__(self):
		super().__init__();
		self.hp = 80
		self.dmg = 5

	def attack(self, plant):
		plant.take_damage(self.dmg)
