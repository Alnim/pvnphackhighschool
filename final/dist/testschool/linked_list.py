# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    linked_list.py                                     :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/22 15:10:36 by wto               #+#    #+#              #
#    Updated: 2018/03/22 15:10:41 by wto              ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

class LinkedList:
	def __init__(self):
		self.head = None

	def add(self, data):
		if (not self.head):
			self.head = ListNode(data)
		else:
			curr = self.head
			while (curr.next):
				curr = curr.next
			curr.next = ListNode(data)

	def is_empty(self):
		return self.head == None

	def remove_beginning(self):
		if (self.head):
			self.head = self.head.next

class ListNode:
	def __init__(self, data):
		self.data = data
		self.next = None
