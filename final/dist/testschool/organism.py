# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    organism.py                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mray <marvin@42.fr>                        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/27 14:40:44 by mray              #+#    #+#              #
#    Updated: 2018/04/05 10:42:54 by mray             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

class Organism:

    def __init__(self):
        self.hp = 35
        self.dmg = 10
        
    def take_damage(self, damage):
        self.hp -= damage
    
#     def get_health(self):
#         return self.hp
