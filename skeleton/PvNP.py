import sys
from plant import Plant
from nonPlant import NonPlant
from queue import Queue
from stack import Stack
from linkedList import LinkedList
from wave import Wave
from game import Game

"""
To-Do:
	- Nothing. Do not modify this file.
	- This file contains the game loop.
"""

if __name__ == "__main__":
	if len(sys.argv) == 2:
		game = Game(sys.argv[1]);
		game.place_wave()
		game.draw()
		game.get_input()
		while (not game.is_over()):
			game.run_turn()
			if (not game.is_over()):
				game.draw()
				game.get_input()
		print("Game Over");
