class Card:
	"""
	A Card codifies a powerup for the plants.
	It stores the powerup to be added to the plants when the card is drawn. 
	To-Do:
		- Define a class variable called 'cost' and set it equal to 5
		- Create an init method that takes in one value
			- Set an instance variable to store the value as the power
		- Create a method called get_power() to return the power of the Card
	"""