class Wave:
	"""
	The Wave class codifies a wave of zombies.
	It stores:
		the turn during which the zombie(s) are released,
		the row in which the zombies are released,
		and the number of zombies to be released.
	To-Do:
		- Create an init method that takes in three values
			- Set an instance variable to store the first value as the turn
			- Set an instance variable to store the second value as the row
			- Set an instance variable to store the third value as the number of zombies
		- Create getter methods for all three instance variables
	"""