class NonPlant:
	"""
	A class representing a NonPlant entity.
	To-Do:
		- Define a class variable called 'worth' and set it equal to 20.
			When the nonplant is killed, the player earns this much cash.
		- Create the init method that takes in no values
			- Set an instance variable to store the health and set it to 80
			- Set an instance variable to store the attack power of the nonplant and set it to 5
		- Create a method called attack() which takes in a plant and applies damage to it (while respecting encapsulation)
		- Create a method called take_damage() which takes in a number and subtracts it from the health
		- Create a method called get_health() which returns the health
	"""