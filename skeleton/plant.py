class Plant:
	"""
	A class representing a Plant entity.
	To-Do:
		- Define a class variable called 'cost' and set it equal to 35.
		- Create the init method that takes in no values
			- Set an instance variable to store the health and set it to 35
			- Set an instance variable to store the attack power of the nonplant and set it to 10
			- Set an instance variable to store the powerup value and set it to 0
		- Create a method called attack() which takes in a nonplant and applies damage to it (while respecting encapsulation)
			- Make sure the powerup is added to the normal damage
		- Create a method called take_damage() which takes in a number and subtracts it from the health
		- Create a method called get_health() which returns the health
		- Create a method called apply_powerup() which takes in a card and adds its value to the plant's existing powerup
		- Create a method called weaken_powerup() which divides the powerup value in half
	"""