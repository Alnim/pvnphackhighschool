from linked_list import LinkedList
from queue import Queue
from stack import Stack
from wave import Wave
from non_plant import NonPlant
from plant import Plant
from card import Card
import random

class Game:
	"""
	A Game codifies the state of the PvNP game.
	It manages the plants, nonplants, the playing field, player cash, and the waves
	To-Do:
		- DO NOT modify existing code.
		- Add to the __init__() method
			- Set an instance variable to store the playing field
			- Initialize each position in the playing field as an empty queue
			- Set an instance variable to store whether the game is over and set it to False
			- Set an instance variable to store the turn number and set it to 0
			- Set an instance variable to store the number of zombies and set it to 0
			- Set an instance variable to store whether the game is over and set it to False
			- Set an instance variable to store a deck of powerup cards and initialize it to an empty stack
		- Create a method called is_over() which returns whether the game is over or not
		- Create a method called get() which takes in a row and col and returns the first entity there (but does not remove it)
		- Create a method called remove() which takes in a row and col and removes the first entity there
			- If the entity is a nonplant, add the requisite amount of cash to the game
			- Handle the nonplant counter appropriately
		- Create a method called place_nonplant() which takes in a row, creates a new nonplant object, and adds it to that row in the last column
			- Handle the nonplant counter appropriately
		- Create a method called place_plant() which takes in a row and col, creates a new plant object, and adds it to that position
			- NOTE: Plants cannot be stacked on top of each other
			- NOTE: Plants cannot be placed in the same location as a nonplant
			- NOTE: Plants cannot be placed in the rightmost column
			- Plants cost money to place, handle appropriately
		- Create a method called place_wave() which takes in no arguments and does the following:
			- Goes through the list of waves
			- For each wave that is equal to the current turn number
				- Release the appropriate number of nonplants in the corresponding row
				- Remove that wave from the beginning of the list
				- Decrement the wave_num counter
		- Create a method called plant_turn() that takes in no arguments and does the following:
			- Goes through the playing field, and for each plant it comes across:
				- Searches for the first nonplant in front of the plant and attacks it
				- If the nonplant's health is <= 0, remove the nonplant from the playing field
		- Create a method called nonplant_turn() that takes in no arguments and does the following:
			- Goes through the playing field, and for each nonplant it comes across:
				- Checks of the nonplant has reached the leftmost column. If it has, the game is over.
					- If it has, the game is over.
					- Print out "The NonPlants Won..."
				- Searches the position in the same row, next column to the left.
				- If this column contains a plant, the nonplant should attack the plant
					- HINT: You can't iterate through a queue, but you know the size
						- All nonplants have the same attack
						- All nonplants in a positon will attack the same plant
				- If the plant's health is <= 0, remove the plant from the playing field
				- If the position in the same row, next column no longer contains a plant:
					- Combine the queue on the left with the current queue
					- Set the current position to an empty queue
		- Create a method called run_turn() which does the following (in this order):
			- Increment the turn counter by 1
			- Weakens all plants' powerups
			- Run the plants' turn
			- Run the nonplants' turn
			- Place any waves necessary
			- Check if the number of nonplants and waves are both zero.
				- If so, the game is over.
				- Draw the game one last time
				- Print out "The Plants Won!" 
		- Create a method called draw_card() that takes in no arguments
			- The method removes the top card from the stack
			- It immediately applies the card's powerup to all existing plants
			- NOTE: Cards cost money. Handle appropriately
	"""
	def __init__(self, file):
		with open(file, 'r') as f:
			self.cash, self.height, self.width = [int(x) for x in f.readline().split(' ')]
			self.waves = LinkedList()
			self.waves_num = 0
			for line in iter(f.readline, ''):
				self.waves.add(Wave(*[int(x) for x in line.split(' ')]))
				self.waves_num += 1
		# WRITE YOUR ADDITIONAL INIT CODE HERE
		for i in range(100):
			self.cards.push(Card(random.randint(0, 5)))

	def draw(self):
		print("Cash: $", self.cash, "\nWaves: ", self.waves_num, sep = '')
		s = '  '.join([str(i) for i in range(self.width - 1)])
		print('  ', s)
		for row in range(self.height):
			s = []
			for col in range(self.width):
				if self.is_plant(row, col):
					char = 'P'
				elif self.is_nonplant(row, col):
					size = self.board[row][col].size()
					char = str(size) if size < 10 else "#"
				else:
					char = '.'
				s.append(char)
			print(row, '  ', '  '.join(s), '\n', sep='')
		print()

	def get_input(self):
		while True:
			ui = input("Action?\n\t[ROW COL] to place plant ($" + 
						str(Plant.cost) + 
						")\n\t[C] to draw a powerup card ($" + 
						str(Card.cost) + 
						")\n\t[Q] to quit\n\t[ENTER] to do nothing?\n")
			if (len(ui) > 0):
				if (len(ui) == 1):
					if (ui.lower() == 'c'):
						self.draw_card()
						break
					elif (ui.lower() == 'q'):
						self.over = True
						break
					else:
						print("Invalid Input \"" + ui + "\"")
				else:
					try:
						row, col = [int(x) for x in ui.split(' ')]
						self.place_plant(row, col)
						break
					except:
						print("Invalid Input \"" + ui + "\"")
			else:
				break

	def is_nonplant(self, row, col):
		return (type(self.get(row, col)).__name__ == 'NonPlant')

	def is_plant(self, row, col):
		return (type(self.get(row, col)).__name__ == 'Plant')

	